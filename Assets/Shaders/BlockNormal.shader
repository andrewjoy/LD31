﻿Shader "Custom/BlockNormal" {
	Properties {
		_Color ("Color", Color) = (1, 1, 1, 1)
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		#pragma surface surf Lambert vertex:vert

		half4 _Color;
		
		float _SelectTime;
		float _DeselectTime;
		
		half getSelectedWeight()
		{
			if (_SelectTime > _DeselectTime)
			{
				return clamp(_Time.y - _SelectTime, 0.0f, 0.5f) * 2.0f;
			}
			else
			{
				return 1.0f - clamp(_Time.y - _DeselectTime, 0.0f, 0.5f) * 2.0f;
			}
		}

		struct Input
		{
			float2 uv_MainTex;
		};
		
		void vert(inout appdata_full v)
		{
			v.vertex.xyz += getSelectedWeight() * normalize(v.vertex) * abs((fmod(_Time.y, 0.5f) * 4.0f) - 1.0f) * 0.5f;
		}

		void surf(Input IN, inout SurfaceOutput o)
		{			
			//o.Albedo = lerp(0.0f, _Color, getSelectedWeight());
			half selectedWeight = getSelectedWeight();
			o.Albedo = 0.0f * (1.0f - selectedWeight) + _Color * selectedWeight;
			
			o.Alpha = 1.0f;
		}
		ENDCG
	} 
	FallBack "Diffuse"
}
