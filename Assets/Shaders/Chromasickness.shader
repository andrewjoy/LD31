﻿Shader "Custom/Chromosickness" {
	Properties {
		_Color ("Color", Color) = (1, 1, 1, 1)
		_MainTex ("Base (RGB)", 2D) = "white" {}
	}
	SubShader {
		ZTest Always
		Cull Off
		ZWrite Off
		Fog { Mode Off }
		
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			half4 _Color;
			sampler2D _MainTex;
			
			float _ShakeX;
			float _ShakeY;

			struct VSInput
			{
				float4 position : POSITION;
				half2 uv : TEXCOORD0;
			};
			
			struct VSOutput
			{
				float4 position : POSITION;
				half2 uv : TEXCOORD0;
			};
			
			VSOutput vert(VSInput input)
			{
				VSOutput output;
				
				output.position = mul(UNITY_MATRIX_MVP, input.position);
				output.position.xy += float2(_ShakeX, _ShakeY);
				output.uv = MultiplyUV(UNITY_MATRIX_TEXTURE0, input.uv);
				
				return output;
			}
			
			half4 frag(VSOutput input) : COLOR
			{
				half4 output;
				
				output.rgb = lerp(tex2D(_MainTex, input.uv).rgb, _Color.rgb, _Color.a);
				output.a = 1.0f;
				
				return output;
			}
			ENDCG
		}
	} 
	FallBack "Diffuse"
}
