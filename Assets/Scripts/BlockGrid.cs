using System.Collections.Generic;

using UnityEngine;

sealed public class BlockGrid : MonoBehaviour
{
    public Vector3 Center
    {
        get
        {
            return center;
        }
    }

    public int Extent
    {
        get
        {
            return extent;
        }
    }

    public float Step
    {
        get
        {
            return step;
        }
    }

    public bool HasBlocks
    {
        get
        {
            return blocks != null;
        }
    }

    private Vector3 center = Vector3.zero;
    private int extent = 0;
    private float step = 0.0f;

    [SerializeField]
    private Block blockPrefab = null;

    private Block[,,] blocks = null;

    public void ConvertBlocksToGroup(Block.Group group, int numberToConvert)
    {
        ConvertBlocksToGroup(group, numberToConvert, extent);
    }

    public void ConvertBlocksToGroup(Block.Group group, int numberToConvert, int maximumExtentFromCenter)
    {
        if (HasBlocks)
        {
            int gridLength = extent * 2 + 1;
            int edgeSize = extent - maximumExtentFromCenter;

            int numberConverted = 0;
            int conversionAttempts = 0;

            while (conversionAttempts < numberToConvert * 4 && numberToConvert > numberConverted)
            {
                ++conversionAttempts;

                int x = Random.Range(edgeSize, gridLength - edgeSize * 2);
                int y = Random.Range(edgeSize, gridLength - edgeSize * 2);
                int z = Random.Range(edgeSize, gridLength - edgeSize * 2);

                if (blocks[x, y, z] != null && blocks[x, y, z].CurrentGroup != group)
                {
                    blocks[x, y, z].CurrentGroup = group;
                    blocks[x, y, z].IsSpecial = Random.Range(0.0f, 1.0f) < group.SpecialChance;

                    ++numberConverted;
                }
            }
        }
    }

    public Block[] GetAdjacentBlocks(Block block, bool getDiagonal)
    {
        if (HasBlocks)
        {
            int gridLength = extent * 2 + 1;
            
            for (int x = 0; x < gridLength; ++x)
            {
                for (int y = 0; y < gridLength; ++y)
                {
                    for (int z = 0; z < gridLength; ++z)
                    {
                        if (blocks[x, y, z] == block)
                        {
                            List<Block> adjacentBlocks = new List<Block>();

                            if (getDiagonal)
                            {
                                for (int xRelative = -1; xRelative <= 1; ++xRelative)
                                {
                                    int xModified = x + xRelative;
                                    if (xModified >= 0 && xModified < gridLength)
                                    {
                                        for (int yRelative = -1; yRelative <= 1; ++yRelative)
                                        {
                                            int yModified = y + yRelative;
                                            if (yModified >= 0 && yModified < gridLength)
                                            {
                                                for (int zRelative = -1; zRelative <= 1; ++zRelative)
                                                {
                                                    int zModified = z + zRelative;
                                                    if (zModified >= 0 && zModified < gridLength)
                                                    {
                                                        if (blocks[xModified, yModified, zModified] != null)
                                                        {
                                                            adjacentBlocks.Add(blocks[xModified, yModified, zModified]);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            else
                            {
                                if (x - 1 >= 0 && blocks[x - 1, y, z] != null)
                                {
                                    adjacentBlocks.Add(blocks[x - 1, y, z]);
                                }

                                if (x + 1 < gridLength && blocks[x + 1, y, z] != null)
                                {
                                    adjacentBlocks.Add(blocks[x + 1, y, z]);
                                }

                                if (y - 1 >= 0 && blocks[x, y - 1, z] != null)
                                {
                                    adjacentBlocks.Add(blocks[x, y - 1, z]);
                                }
                                
                                if (y + 1 < gridLength && blocks[x, y + 1, z] != null)
                                {
                                    adjacentBlocks.Add(blocks[x, y + 1, z]);
                                }

                                if (z - 1 >= 0 && blocks[x, y, z - 1] != null)
                                {
                                    adjacentBlocks.Add(blocks[x, y, z - 1]);
                                }
                                
                                if (z + 1 < gridLength && blocks[x, y, z + 1] != null)
                                {
                                    adjacentBlocks.Add(blocks[x, y, z + 1]);
                                }
                            }
                            
                            return adjacentBlocks.ToArray();
                        }
                    }
                }
            }
        }

        return new Block[0];
    }

    public Block GetBlockAt(int x, int y, int z)
    {
        if (HasBlocks)
        {
            return blocks[x, y, z];
        }

        return null;
    }

    public void GenerateBlocks(Vector3 center, int extent, float step, Block.Group[] groups)
    {
        ClearBlocks();

        this.center = center;
        this.extent = extent;
        this.step = step;

        int gridLength = extent * 2 + 1;

        blocks = new Block[gridLength, gridLength, gridLength];

        for (int x = -extent; x <= extent; ++x)
        {
            for (int y = -extent; y <= extent; ++y)
            {
                for (int z = -extent; z <= extent; ++z)
                {
                    Block newBlock = (Block)Instantiate(blockPrefab, center + new Vector3(x * step, y * step, z * step), Quaternion.identity);

                    newBlock.CurrentGroup = groups[Random.Range(0, groups.Length)];
                    newBlock.IsSpecial = Random.Range(0.0f, 1.0f) < newBlock.CurrentGroup.SpecialChance;

                    blocks[x + extent, y + extent, z + extent] = newBlock;
                }
            }
        }
    }

    public void ClearBlocks()
    {
        if (HasBlocks)
        {
            int gridLength = extent * 2 + 1;

            for (int x = 0; x < gridLength; ++x)
            {
                for (int y = 0; y < gridLength; ++y)
                {
                    for (int z = 0; z < gridLength; ++z)
                    {
                        if (blocks[x, y, z] != null)
                        {
                            Destroy(blocks[x, y, z].gameObject);
                        }
                    }
                }
            }

            center = Vector3.zero;
            extent = 0;
            step = 0;
            
            blocks = null;
        }
    }
}