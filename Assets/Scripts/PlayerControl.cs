﻿using System;
using System.Collections.Generic;

using UnityEngine;

[RequireComponent(typeof(Camera))]
sealed public class PlayerControl : MonoBehaviour
{
	public enum Stage
	{
		Setup = 0,
		Play = 1,
		Victory = 2,
		Defeat = 3
	}

    public enum Difficulty
    {
        Easy = 1,
        Normal = 2
    }
	
	private struct DelayedDestructionBlocks
	{
		public float time;
		public Block[] blocks;
		
		public DelayedDestructionBlocks(float time, Block[] blocks)
		{
			this.time = time;
			this.blocks = blocks;
		}
	}
	
	public Stage CurrentStage
	{
		get
		{
			return currentStage;
		}
		
		private set
		{
			if (currentStage != value)
			{
				switch (currentStage)
				{
				case Stage.Setup:
					
					
					break;
				case Stage.Play :
					Screen.lockCursor = false;
					
					laser.enabled = false;
					
					break;
				case Stage.Victory :
					
					
					break;
				case Stage.Defeat :
					
					
					break;
				}
				
				currentStage = value;
				
				switch (currentStage)
				{
				case Stage.Setup:
					
					
					break;
				case Stage.Play :
                    switch (currentDifficulty)
                    {
                        case Difficulty.Easy:
                            TestFairy.SetCheckpoint(TestCheckpoint.PlayEasy);

                            break;
                        case Difficulty.Normal:
                            TestFairy.SetCheckpoint(TestCheckpoint.PlayNormal);

                            break;
                    }

					chargePoints = 0;
					
					chromashiftCount = 0;
					lastChromashiftChargePoints = 0;
					lastChromashiftTime = Time.timeSinceLevelLoad;
					
					bonusChargeMultiplier = 0.0f;
					
					for (int i = 0; i < playObjects.Count; ++i)
					{
						if (playObjects[i] != null)
						{
							Destroy(playObjects[i]);
						}
					}
					
					playObjects.Clear();

                    grid.GenerateBlocks(Vector3.zero, gridExtent, gridStep, gridGroups.ToArray());

                    objectiveBlocksTotal = -objectiveGridGroup.MemberCount;
                    grid.ConvertBlocksToGroup(objectiveGridGroup, grid.Extent * 2, Mathf.Max(grid.Extent - 2, Mathf.Min(grid.Extent, 3)));
					objectiveBlocksTotal += objectiveGridGroup.MemberCount;
					
					TargetGroup = null;
					
					for (int i = 0; i < gridGroups.Count; ++i)
					{
						gridGroups[i].IsSelected = true;
						gridGroups[i].IsSelected = false;
					}
                    
					TargetGroup = gridGroups[0];
					
					transform.position = grid.Center + new Vector3(1.0f, 0.0f, 1.0f).normalized * (grid.Extent * 2 + 1) * 2;
					transform.rotation = Quaternion.LookRotation(grid.Center - transform.position);
					
					Screen.lockCursor = true;
					
					laser.enabled = true;
					laserSizeBoost = 0.0f;
					laserTargetOverride = null;
					
					playTime = 0.0f;
					slayerBonusCount = 0;
					indiscriminateBonusCount = 0;
					
					break;
				case Stage.Victory :
                    TestFairy.SetCheckpoint(TestCheckpoint.Victory);
					
					break;
				case Stage.Defeat :
                    TestFairy.SetCheckpoint(TestCheckpoint.Defeat);
					
					break;
				}
			}
		}
	}

    public Difficulty CurrentDifficulty
    {
        get
        {
            return currentDifficulty;
        }

        private set
        {
            currentDifficulty = value;
        }
    }
	
	public BlockGrid Grid
	{
		get
		{
			return grid;
		}
	}
	
	public float LaserSizeBoost
	{
		get
		{
			return laserSizeBoost;
		}
		
		set
		{
			laserSizeBoost = value;
		}
	}
	
	public Vector3? LaserTargetOverride
	{
		get
		{
			return laserTargetOverride;
		}
		
		set
		{
			laserTargetOverride = value;
		}
	}
	
	public Block.Group TargetGroup
	{
		get
		{
			return targetGroup;
		}
		
		set
		{
			if (targetGroup != value)
			{
				if (targetGroup != null)
				{
					targetGroup.IsSelected = false;
				}
				
				targetGroup = value;
				
				if (targetGroup != null)
				{
					targetGroup.IsSelected = true;
					laser.SetColors(targetGroup.Color, targetGroup.Color);
				}
			}
		}
	}
	
	public float ChargeMultiplier
	{
		get
		{
			return 1.0f + chromashiftCount * 0.1f + ChromasicknessWeight + bonusChargeMultiplier;
		}
	}
	
	public int ChargePoints
	{
		get
		{
			return chargePoints;
		}
		
		set
		{
			if (currentStage == Stage.Play)
			{
				if (value > chargePoints)
				{
					chargePoints += (int)((value - chargePoints) * ChargeMultiplier);
				}
				else
				{
					chargePoints = value;
				}
			}
		}
	}
	
	public float ShakeTime
	{
		get
		{
			return shakeTime;
		}
		
		set
		{
			shakeTime = value;
		}
	}

    public float ChromasicknessWeight
    {
        get
        {
            float difficultySpeed = 1.0f;

            switch (currentDifficulty)
            {
                case Difficulty.Easy :
                    difficultySpeed = 0.7f;

                    break;
                case Difficulty.Normal :
                    difficultySpeed = 1.0f;

                    break;
            }

            return Mathf.Clamp01((Time.timeSinceLevelLoad - lastChromashiftTime) / (chromasicknessTime / difficultySpeed));
        }
    }
	
	private Stage currentStage = Stage.Setup;

    private Difficulty currentDifficulty = Difficulty.Normal;
	
	[SerializeField]
	private GUISkin guiSkin = null;
	
	[SerializeField]
	private BlockGrid grid = null;
	
	[SerializeField]
	private int gridExtent = 6;
	
	[SerializeField]
	private float gridStep = 1.2f;
	
	[SerializeField]
	private List<Block.Group> gridGroups = null;
	
	[SerializeField]
	private Block.Group objectiveGridGroup = null;
	
	private int objectiveBlocksTotal = 0;
	
	[SerializeField]
	private float blockDestructionDelay = 0.2f;
	
	private readonly Queue<DelayedDestructionBlocks> delayedDestructionBlocks = new Queue<DelayedDestructionBlocks>();
	
	[SerializeField]
	private LineRenderer laser = null;
	
	[SerializeField]
	private float laserStartWidth = 0.025f;
	
	[SerializeField]
	private float laserEndWidth = 5.0f;
	
	[SerializeField]
	private float laserDistance = 1000.0f;
	
	private float laserSizeBoost = 0.0f;
	
	[SerializeField]
	private Vector3? laserTargetOverride = null;
	
	[SerializeField]
	private float flickerTime = 0.2f;
	
	private Block.Group targetGroup = null;
	
	[SerializeField]
	private int chromashiftChargePoints = 20000;

    [SerializeField]
    private int chromashiftChargePointsPerExtent = 8000;

    [SerializeField]
    private int chromashiftChargePointStepPerExtent = 8000;
	
	private int chromashiftCount = 0;
	private int lastChromashiftChargePoints = 0;
	private float lastChromashiftTime = 0.0f;
	
	[SerializeField]
	private Shader chromasicknessShader = null;
	
	private Material chromasicknessMaterial = null;
	
	[SerializeField]
	private float chromasicknessTime = 20.0f;
	
	private int chargePoints = 0;
	private int visibleChargePoints = 0;

    [SerializeField]
    private int slayerBonusPoints = 1000;

    private int slayerBonusCount = 0;

    [SerializeField]
    private int indiscriminateBonusPoints = 5000;

    private int indiscriminateBonusCount = 0;
	
	[SerializeField]
	private float lastSecondBonusMultiplier = 0.2f;
	
	private float bonusChargeMultiplier = 0.0f;
	private float playTime = 0.0f;

    [SerializeField]
    private float specialBlockBountyMultiplier = 5.0f;
	
	private readonly List<GameObject> playObjects = new List<GameObject>();
	
	private readonly Queue<string> messages = new Queue<string>();
	private float messageTimeTotal = 2.0f;
	private float messageTimeCurrent = 0.0f;
	
	[SerializeField]
	private float shakeMaximum = 0.015f;
	
	[SerializeField]
	private float shakeSpeed = 0.05f;
	
	private float shakeTime = 0.0f;
	private float shakeWeight = 0.0f;
	
	private Vector2 shakeDirection = Vector2.zero;
	private Vector2 shakePosition = Vector2.zero;
	
	[SerializeField]
	private float keyboardRotationSpeed = 1.0f;
	
	[SerializeField]
	private float mouseRotationSpeed = 1.0f;
	
	[SerializeField]
	private float touchRotationSpeed = 1.0f;
	
	[SerializeField]
	private float touchTapTime = 0.2f;
	
	private Vector2 touchPreviousPosition = Vector2.zero;
	private Vector2 touchRotationRate = Vector2.zero;
	private float touchTime = 0.0f;
    private int touchFrame = 0;
	
	public void AddPlayObject(GameObject playObject)
	{
		if (!playObjects.Contains(playObject))
		{
			playObjects.Add(playObject);
		}
	}
	
	public PREFAB_TYPE InstantiatePlayObject<PREFAB_TYPE>(PREFAB_TYPE prefab, Vector3 position, Quaternion rotation)
		where PREFAB_TYPE : MonoBehaviour
	{
		PREFAB_TYPE instantiatedPrefab = (PREFAB_TYPE)Instantiate(prefab, position, rotation);
		
		playObjects.Add(instantiatedPrefab.gameObject);
		
		return instantiatedPrefab;
	}
	
	public void DestroyBlock(Block block)
	{
		Block[] adjacentBlocks = grid.GetAdjacentBlocks(block, false);
		
		for (int i = 0; i < adjacentBlocks.Length; ++i)
		{
			if (adjacentBlocks[i].CurrentGroup != block.CurrentGroup || adjacentBlocks[i].IsSpecial)
			{
				adjacentBlocks[i] = null;
			}
		}
		
		if (block.CurrentGroup != null)
		{
			block.CurrentGroup.PlayDestructionEffects(block.gameObject.transform.position);
			
			ChargePoints += (int)(block.CurrentGroup.ChargePointBounty * (block.IsSpecial ? specialBlockBountyMultiplier : 1.0f));

            if (CurrentStage == Stage.Play)
            {
                if (block.CurrentGroup.IsSelected)
                {
                    ++slayerBonusCount;

                    int slayerBonusThreshold = 40 * grid.Extent;

                    if (slayerBonusCount >= slayerBonusThreshold)
                    {
                        messages.Enqueue("Slayer! +" + slayerBonusPoints);

                        ChargePoints += slayerBonusPoints;

                        slayerBonusCount -= slayerBonusThreshold;

                        shakeTime += 0.2f;
                    }

                    if (block.CurrentGroup == objectiveGridGroup)
                    {
                        shakeTime += 0.3f;
                    }
                }
                else
                {
                    ++indiscriminateBonusCount;

                    int indeiscriminateBonusTreshold = 25 * grid.Extent;

                    if (indiscriminateBonusCount % indeiscriminateBonusTreshold == 0)
                    {
                        messages.Enqueue("Indisciriminate! +" + indiscriminateBonusPoints);

                        ChargePoints += indiscriminateBonusPoints;

                        shakeTime += 0.2f;
                    }
                }
            }
		}
		
		Destroy(block.gameObject);
		
		if (adjacentBlocks.Length > 0)
		{
			delayedDestructionBlocks.Enqueue(new DelayedDestructionBlocks(Time.timeSinceLevelLoad, adjacentBlocks));
		}
	}

    private int CalculateChromashiftCost()
    {
        return (int)((chromashiftChargePoints + (chromashiftChargePointStepPerExtent * chromashiftCount + chromashiftChargePointsPerExtent) * grid.Extent) * (Application.isMobilePlatform ? 0.7f : 1.0f));
    }
	
	private void Start()
	{
		if (Application.isMobilePlatform)
		{
			gridExtent -= 2;
		}
	}
	
	private void Update()
	{
		switch (currentStage)
		{
		case Stage.Setup :
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                Application.Quit();
            }
			
			break;
		case Stage.Play :
			playTime += Time.deltaTime;
			
			Screen.lockCursor = true;
			
			transform.position = grid.Center;
			
			if (Application.isMobilePlatform)
			{
				if (Input.touchCount > 0)
				{
					Touch touch = Input.GetTouch(0);

                    if (touch.phase == TouchPhase.Began)
                    {
                        touchTime = Time.timeSinceLevelLoad;
                        touchFrame = Time.frameCount;
                    }
                    else
					{
						touchRotationRate += touch.position - touchPreviousPosition;
					}

					touchPreviousPosition = touch.position;
				}
				
				touchRotationRate = Vector2.Lerp(touchRotationRate, Vector3.zero, Time.deltaTime * 2.0f);
				
				transform.rotation *= Quaternion.Euler(-(touchRotationRate.y / Screen.height) * touchRotationSpeed * Time.deltaTime, (touchRotationRate.x / Screen.width) * touchRotationSpeed * Time.deltaTime, 0.0f);
			}
			else
			{
				transform.rotation *= Quaternion.Euler((Input.GetAxis("Mouse Y") * mouseRotationSpeed + Input.GetAxis("Vertical") * keyboardRotationSpeed) * Time.deltaTime, (-Input.GetAxis("Mouse X") * mouseRotationSpeed - Input.GetAxis("Horizontal") * keyboardRotationSpeed) * Time.deltaTime, 0.0f);
			}
			
			transform.position -= transform.forward * (grid.Extent * 2 + 1) * 2 * grid.Step;
			
			RaycastHit hit;
			if (Physics.SphereCast(camera.ScreenPointToRay(camera.WorldToScreenPoint(laserTargetOverride ?? grid.Center)), 0.1f, out hit))
			{
				Collider[] colliders = Physics.OverlapSphere(hit.point, 0.5f + 0.2f * laserSizeBoost);
				
				for (int i = 0; i < colliders.Length; ++i)
				{
					Block blockComponent = colliders[i].gameObject.GetComponent<Block>();
					if (blockComponent != null && (blockComponent.CurrentGroup != objectiveGridGroup || hit.transform.gameObject == blockComponent.gameObject))
					{
						if (blockComponent.CurrentGroup == targetGroup || blockComponent.CurrentGroup == objectiveGridGroup)
						{
							if (blockComponent.IsSpecial && blockComponent.CurrentGroup != null)
							{
								blockComponent.CurrentGroup.SpecialAction.Perform(blockComponent);
							}
							else
							{
								DestroyBlock(blockComponent);
							}
						}
					}
				}
			}
			
			while (delayedDestructionBlocks.Count > 0 && (Time.timeSinceLevelLoad - delayedDestructionBlocks.Peek().time) >= blockDestructionDelay)
			{
				Block[] blocks = delayedDestructionBlocks.Dequeue().blocks;
				for (int i = 0; i < blocks.Length; ++i)
				{
					if (blocks[i] != null && !blocks[i].IsSpecial)
					{
						DestroyBlock(blocks[i]);
					}
				}
			}
			
			float scaledLaserEndWidth = laserEndWidth;
			
			if (laserTargetOverride != null)
			{
				scaledLaserEndWidth *= Vector3.Distance((Vector3)laserTargetOverride, transform.position) / laserDistance;
			}
			
			laser.SetWidth(
				(1.0f + laserSizeBoost) * (laserStartWidth + laserStartWidth * Mathf.Abs((Time.time % flickerTime) * (2.0f / flickerTime) - 1.0f)), 
				(1.0f + laserSizeBoost) * (scaledLaserEndWidth + scaledLaserEndWidth * Mathf.Abs(((Time.time % flickerTime) * (2.0f / flickerTime) - 1.0f)))
				);
			
			visibleChargePoints = (int)Mathf.Lerp(visibleChargePoints, chargePoints, Time.deltaTime * (visibleChargePoints < chargePoints ? 2.0f : 4.0f));
			
			if (chargePoints - lastChromashiftChargePoints >= CalculateChromashiftCost())
			{
				if (Application.isMobilePlatform 
				    ? (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Ended && ((Time.timeSinceLevelLoad - touchTime) <= touchTapTime || touchFrame == Time.frameCount - 1))
				    : (Input.GetMouseButtonDown(0) || Input.GetKeyDown(KeyCode.Space)))
				{
					++chromashiftCount;
					
					if (chromasicknessTime - (Time.timeSinceLevelLoad - lastChromashiftTime) <= 1.0f)
					{
						bonusChargeMultiplier += lastSecondBonusMultiplier;
						
						messages.Enqueue("Last Second! Nice! + x0.2");

                        TestFairy.SetCheckpoint(TestCheckpoint.LastSecondShift);
					}
					
					lastChromashiftChargePoints = chargePoints;
					lastChromashiftTime = Time.timeSinceLevelLoad;
					
					TargetGroup = gridGroups[(gridGroups.IndexOf(targetGroup) + 1) % gridGroups.Count];
					
					slayerBonusCount = 0;
				}
			}
			
			laser.SetPosition(1, laserTargetOverride == null ? new Vector3(0.0f, 0.0f, laserDistance) : (Quaternion.Inverse(transform.rotation) * ((Vector3)laserTargetOverride - transform.position)));
			
			if (objectiveGridGroup.MemberCount <= 0)
			{
				CurrentStage = Stage.Victory;
			}
			
			if (ChromasicknessWeight >= 1.0f)
			{
				CurrentStage = Stage.Defeat;
			}

            if (Input.GetKeyDown(KeyCode.Escape))
            {
                CurrentStage = Stage.Defeat;
                CurrentStage = Stage.Setup;
            }
			
			break;
		case Stage.Victory :
			
			
			break;
		case Stage.Defeat :
			
			
			break;
		}
		
		if (messages.Count > 0)
		{
			messageTimeCurrent += Time.deltaTime;
			
			if (messageTimeCurrent > messageTimeTotal)
			{
				messageTimeCurrent = 0.0f;
				messages.Dequeue();
			}
		}
		
		shakeTime -= Time.deltaTime;
		
		if (shakeTime <= 0.0f)
		{
			shakeTime = 0.0f;
			
			shakeWeight -= Time.deltaTime * 5.0f;
			
			if (shakeWeight < 0.0f)
			{
				shakeWeight = 0.0f;
			}
		}
		else
		{
			shakeWeight += Time.deltaTime * 5.0f;
			
			if (shakeWeight > 1.0f)
			{
				shakeWeight = 1.0f;
			}
		}
		
		if (Time.timeSinceLevelLoad % shakeSpeed < Time.deltaTime)
		{
			shakeDirection = new Vector2(UnityEngine.Random.Range(-1.0f, 1.0f), UnityEngine.Random.Range(-1.0f, 1.0f));
			
			shakeDirection.x = shakeDirection.x >= 0.0f ? 1.0f : -1.0f;
			shakeDirection.y = shakeDirection.y >= 0.0f ? 1.0f : -1.0f;
		}
		
		shakePosition = new Vector3(
			Mathf.Clamp(shakePosition.x + shakeDirection.x * Time.deltaTime, -shakeMaximum, shakeMaximum), 
			Mathf.Clamp(shakePosition.y + shakeDirection.y * Time.deltaTime, -shakeMaximum, shakeMaximum)
			);
	}
	
	private void OnGUI()
	{
		GUI.skin = guiSkin;
		
		switch (currentStage)
		{
		case Stage.Setup :
			GUI.color = objectiveGridGroup.Color;
			
			GUI.Box(new Rect(Screen.width * 0.5f - 205.0f, Screen.height * 0.5f - 155.0f, 410.0f, 310.0f), "");
			
			GUI.color = Color.cyan;
			
			GUI.Box(new Rect(Screen.width * 0.5f - 200.0f, Screen.height * 0.5f - 150.0f, 400.0f, 300.0f), "");
			
			GUI.color = objectiveGridGroup.Color;
			
			GUI.Label(new Rect(Screen.width * 0.5f - 150.0f, Screen.height * 0.5f - 130.0f, 60.0f, 30.0f), "Size:");
			gridExtent = (int)GUI.HorizontalSlider(new Rect(Screen.width * 0.5f - 150.0f, Screen.height * 0.5f - 90.0f, 300.0f, 30.0f), gridExtent, 3.0f, Application.isMobilePlatform ? 8.0f : 10.0f);
			GUI.Label(new Rect(Screen.width * 0.5f - 90.0f, Screen.height * 0.5f - 130.0f, 40.0f, 30.0f), gridExtent.ToString());
			
			if (GUI.Button(new Rect(Screen.width * 0.5f - 150.0f, Screen.height * 0.5f - 50.0f, 300.0f, 80.0f), "Play Easy!"))
			{
				CurrentStage = Stage.Play;
                CurrentDifficulty = Difficulty.Easy;
			}

            if (GUI.Button(new Rect(Screen.width * 0.5f - 150.0f, Screen.height * 0.5f + 50.0f, 300.0f, 80.0f), "Play Normal!"))
            {
                CurrentStage = Stage.Play;
                CurrentDifficulty = Difficulty.Normal;
            }
			
			break;
		case Stage.Play :
			GUIStyle chargePointsStyle = new GUIStyle(GUI.skin.label);
			
			chargePointsStyle.alignment = TextAnchor.UpperCenter;
			chargePointsStyle.fontStyle = FontStyle.Bold;
			chargePointsStyle.fontSize += Mathf.Max((int)(Mathf.Log((chargePoints - visibleChargePoints) / 400.0f) * 20.0f), 0);
			
			GUI.color = Color.Lerp(Color.cyan, objectiveGridGroup.Color, Mathf.Clamp((chargePoints - visibleChargePoints) / 10000.0f, 0.0f, 1.0f));
			
			GUI.Label(new Rect(Screen.width * 0.5f - 400.0f, Screen.height * 0.1f, 800.0f, 300.0f), visibleChargePoints.ToString(), chargePointsStyle);
			
			chargePointsStyle.fontSize = 32;
			
			GUI.Label(new Rect(Screen.width * 0.5f + 100.0f, Screen.height * 0.1f, 200.0f, 300.0f), "x" + ChargeMultiplier.ToString("F1"), chargePointsStyle);
			
			Rect chromashiftRect = new Rect(Screen.width * 0.5f - 150.0f, Screen.height * 0.1f - 30.0f, 300.0f, 30.0f);
			
			chargePointsStyle.alignment = TextAnchor.MiddleCenter;
			chargePointsStyle.fontSize = 20;
			
			if (chargePoints - lastChromashiftChargePoints >= CalculateChromashiftCost())
			{
				bool shouldInvert = Time.time % 2.0f >= 1.0f;
				
				GUI.color = shouldInvert ? Color.cyan : objectiveGridGroup.Color;
				
				GUI.Box(chromashiftRect, "");
				
				GUI.color = shouldInvert ? objectiveGridGroup.Color : Color.cyan;
				
				GUI.Box(new Rect(chromashiftRect.x + 5.0f, chromashiftRect.y + 5.0f, chromashiftRect.width - 10.0f, chromashiftRect.height - 10.0f), "");
				
				GUI.color = shouldInvert ? Color.cyan : objectiveGridGroup.Color;
				
				GUI.Label(chromashiftRect, (Application.isMobilePlatform ? "Tap" : "Click") + " to Chromashift!", chargePointsStyle);
			}
			else
			{
				GUI.color = Color.cyan;
				
				GUI.Box(chromashiftRect, "");
				
				GUI.color = objectiveGridGroup.Color;
				
				GUI.Label(chromashiftRect, "Charging Chromashift...", chargePointsStyle);
				
				GUI.BeginGroup(new Rect(chromashiftRect.x, chromashiftRect.y, chromashiftRect.width * ((chargePoints - lastChromashiftChargePoints) / (float)CalculateChromashiftCost()), chromashiftRect.height));
				
				GUI.Box(new Rect(5.0f, 5.0f, chromashiftRect.width - 10.0f, chromashiftRect.height - 10.0f), "");
				
				GUI.color = Color.cyan;
				
				GUI.Label(new Rect(0.0f, 0.0f, chromashiftRect.width, chromashiftRect.height), "Charging Chromashift...", chargePointsStyle);
				
				GUI.EndGroup();
			}
			
			for (int i = 0; i < objectiveBlocksTotal; ++i)
			{
				Rect boxRect = new Rect(Screen.width * 0.5f - 30.0f * grid.Extent + 5.0f + 30.0f * i, (Screen.height * 0.1f - 30.0f) * 0.5f - 10.0f, 20.0f, 20.0f);
				
				GUI.color = Color.cyan;
				
				GUI.Box(new Rect(boxRect.x - 5.0f, boxRect.y - 5.0f, boxRect.width + 10.0f, boxRect.height + 10.0f), "");
				
				if (i < objectiveGridGroup.MemberCount)
				{
					GUI.color = objectiveGridGroup.Color;
					
					GUI.Box(boxRect, "");
				}
			}
			
			break;
		case Stage.Victory :
			GUI.color = objectiveGridGroup.Color;
			
			GUI.Box(new Rect(Screen.width * 0.5f - 205.0f, Screen.height * 0.5f - 215.0f, 410.0f, 210.0f), "");
			
			GUI.Box(new Rect(Screen.width * 0.5f - 205.0f, Screen.height * 0.5f + 5.0f, 410.0f, 210.0f), "");
			
			GUI.color = Color.cyan;
			
			GUI.Box(new Rect(Screen.width * 0.5f - 200.0f, Screen.height * 0.5f - 210.0f, 400.0f, 200.0f), "");
			
			GUI.Box(new Rect(Screen.width * 0.5f - 200.0f, Screen.height * 0.5f + 10.0f, 400.0f, 200.0f), "");
			
			GUI.color = objectiveGridGroup.Color;
			
			GUI.Label(new Rect(Screen.width * 0.5f - 180.0f, Screen.height * 0.5f - 200.0f, 360.0f, 180.0f), "You scored " + chargePoints + " points\nin " + playTime.ToString("F1") + " seconds.\nThat's " + (chargePoints / playTime).ToString("F1") + " points a second!");
			
			GUI.Label(new Rect(Screen.width * 0.5f - 150.0f, Screen.height * 0.5f + 20.0f, 300.0f, 80.0f), "You Win!\nGood on you.");
			
			if (GUI.Button(new Rect(Screen.width * 0.5f - 100.0f, Screen.height * 0.5f + 110.0f, 200.0f, 80.0f), "Play Again!"))
			{
				CurrentStage = Stage.Setup;
			}
			
			break;
		case Stage.Defeat :
			GUI.color = objectiveGridGroup.Color;
			
			GUI.Box(new Rect(Screen.width * 0.5f - 205.0f, Screen.height * 0.5f - 105.0f, 410.0f, 210.0f), "");
			
			GUI.color = Color.cyan;
			
			GUI.Box(new Rect(Screen.width * 0.5f - 200.0f, Screen.height * 0.5f - 100.0f, 400.0f, 200.0f), "");
			
			GUI.color = objectiveGridGroup.Color;
			
			GUI.Label(new Rect(Screen.width * 0.5f - 150.0f, Screen.height * 0.5f - 80.0f, 300.0f, 80.0f), "You Got Chromasick!\nWhat a pity.");
			
			if (GUI.Button(new Rect(Screen.width * 0.5f - 100.0f, Screen.height * 0.5f, 200.0f, 80.0f), "Play Again!"))
			{
				CurrentStage = Stage.Setup;
			}
			
			break;
		}
		
		if (messages.Count > 0)
		{
			float weight = (1.0f - Mathf.Clamp01((messageTimeCurrent - (messageTimeTotal - 1.0f)) / 1.0f));
			
			GUIStyle messageStyle = new GUIStyle(GUI.skin.label);
			
			messageStyle.alignment = TextAnchor.UpperCenter;
			messageStyle.fontStyle = FontStyle.Bold;
			messageStyle.fontSize = (int)(48 * weight);

			float drawWidth = 600.0f;

			if (drawWidth > Screen.width * 0.9f)
			{
				drawWidth = Screen.width * 0.9f;
			}
			
			GUI.color = Color.cyan;
			
			GUI.Label(new Rect(Screen.width * 0.5f - drawWidth * 0.5f, Screen.height * 0.5f - 200.0f - (1.0f - weight) * 400.0f, drawWidth, 400.0f), messages.Peek(), messageStyle);
		}
	}
	
	private void OnRenderImage(RenderTexture source, RenderTexture destination)
	{
		if (chromasicknessMaterial == null || chromasicknessMaterial.shader != chromasicknessShader)
		{
			chromasicknessMaterial = new Material(chromasicknessShader);
		}
		
		float chromasicknessWeight = ChromasicknessWeight;
		
		if (targetGroup != null)
		{
			if (currentStage == Stage.Play)
			{
				chromasicknessMaterial.color = new Color(targetGroup.Color.r, targetGroup.Color.g, targetGroup.Color.b, chromasicknessWeight);
			}
		}
		else
		{
			chromasicknessMaterial.color = Color.clear;
		}
		
		chromasicknessMaterial.SetFloat("_ShakeX", shakePosition.x * (shakeWeight + chromasicknessWeight * chromasicknessWeight * chromasicknessWeight));
		chromasicknessMaterial.SetFloat("_ShakeY", shakePosition.y * (shakeWeight + chromasicknessWeight * chromasicknessWeight * chromasicknessWeight));
		
		Graphics.Blit(source, destination, chromasicknessMaterial);
	}
}
