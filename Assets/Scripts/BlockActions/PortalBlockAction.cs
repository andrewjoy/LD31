﻿using UnityEngine;

sealed public class PortalBlockAction : BlockAction
{
    [SerializeField]
    private PlayerControl playerControl = null;

    [SerializeField]
    private Turret portalPrefab = null;

    [SerializeField]
    private AudioClip soundClip = null;

    public override void Perform(Block performer)
    {
        Vector3 displacementNormalized = (performer.gameObject.transform.position - playerControl.Grid.Center).normalized;

        Turret portal = playerControl.InstantiatePlayObject(portalPrefab, playerControl.Grid.Center + displacementNormalized * playerControl.Grid.Extent * playerControl.Grid.Step * 2.0f, Quaternion.LookRotation(-displacementNormalized));

        portal.Owner = playerControl;

        playerControl.DestroyBlock(performer);

        AudioSource.PlayClipAtPoint(soundClip, performer.gameObject.transform.position);
    }
}
