﻿using System.Collections.Generic;

using UnityEngine;

sealed public class ChargeBlockAction : BlockAction
{
    [SerializeField]
    private int chargePoints = 2000;

    [SerializeField]
    private PlayerControl playerControl = null;

    [SerializeField]
    private AudioClip soundClip = null;

    public override void Perform(Block performer)
    {
        playerControl.DestroyBlock(performer);

        playerControl.ChargePoints += chargePoints;

        AudioSource.PlayClipAtPoint(soundClip, performer.gameObject.transform.position);
    }
}
