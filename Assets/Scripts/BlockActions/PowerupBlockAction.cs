﻿using UnityEngine;

sealed public class PowerupBlockAction : BlockAction
{
    [SerializeField]
    private PlayerControl playerControl = null;

    [SerializeField]
    private float sizeIncrease = 0.2f;

    [SerializeField]
    private AudioClip soundEffect = null;

    public override void Perform(Block performer)
    {
        playerControl.LaserSizeBoost += sizeIncrease;

        AudioSource.PlayClipAtPoint(soundEffect, performer.gameObject.transform.position);

        playerControl.DestroyBlock(performer);
    }
}
