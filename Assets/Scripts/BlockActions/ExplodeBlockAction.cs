﻿using UnityEngine;

sealed public class ExplodeBlockAction : BlockAction
{
    [SerializeField]
    private PlayerControl playerControl = null;

    public override void Perform(Block performer)
    {
        playerControl.DestroyBlock(performer);

        Block[] blocksToExplode = playerControl.Grid.GetAdjacentBlocks(performer, true);
        for (int i = 0; i < blocksToExplode.Length; ++i)
        {
            playerControl.DestroyBlock(blocksToExplode[i]);
        }
    }
}
