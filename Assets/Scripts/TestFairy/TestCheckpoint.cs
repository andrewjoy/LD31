﻿public enum TestCheckpoint
{
    PlayEasy,
    PlayNormal,
    Victory,
    Defeat,
    LastSecondShift
}