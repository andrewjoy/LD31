﻿using System;

using UnityEngine;

/// <summary>
/// Wrapper functions for the <see href="http://testfairy.com/" mobile testing platform./> 
/// </summary>
static public class TestFairy
{
	/// <summary>
    /// Add a <see href="http://docs.testfairy.com/Advanced/Checkpoints.html">checkpoint</see> that TestFairy can track.
    /// </summary>
    /// <param name="checkpointName">Name of the checkpoint.</param>
    static public void SetCheckpoint(string checkpointName)
	{
#if !UNITY_EDITOR
#if UNITY_ANDROID
        AndroidJavaObject logObject = new AndroidJavaObject("android.util.Log");
		logObject.CallStatic<int>("v", "testfairy-checkpoint", checkpointName);
#elif UNITY_IPHONE

#endif
#endif
	}

    /// <summary>
    /// Add a <see href="http://docs.testfairy.com/Advanced/Checkpoints.html">checkpoint</see> that TestFairy can track.
    /// </summary>
    /// <param name="checkpointName">Name of the checkpoint.</param>
    static public void SetCheckpoint(Enum value)
    {
        SetCheckpoint(value.ToString());
    }

    /// <summary>
    /// Adds a <see href="http://docs.testfairy.com/Advanced/Correlation_Id.html">correlation ID</see> that TestFairy can track.
    /// </summary>
    /// <param name="correlationValue">Text value to correlate.</param>
    static public void SetCorrelationID(string correlationValue)
    {
#if !UNITY_EDITOR
#if UNITY_ANDROID
        AndroidJavaObject logObject = new AndroidJavaObject("android.util.Log");
        logObject.CallStatic<int>("d", "testfairy-correlation-id", correlationValue);
#elif UNITY_IPHONE

#endif
#endif
    }
}
