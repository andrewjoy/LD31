﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
sealed public class Projectile : MonoBehaviour
{
    public PlayerControl Owner
    {
        get
        {
            return owner;
        }

        set
        {
            owner = value;
        }
    }

    [SerializeField]
    private float movementSpeed = 5.0f;

    [SerializeField]
    private AudioClip soundEffect = null;

    private PlayerControl owner = null;

    private void Start()
    {
        AudioSource.PlayClipAtPoint(soundEffect, transform.position);
    }

    private void Update()
    {
        transform.position += transform.rotation * new Vector3(0.0f, 0.0f, movementSpeed * Time.deltaTime);
    }

    private void OnCollisionEnter(Collision collision)
    {
        Block blockComponent = collision.gameObject.GetComponent<Block>();
        if (blockComponent != null)
        {
            owner.DestroyBlock(blockComponent);
        }

        Destroy(gameObject);
    }
}
