﻿using System;

using UnityEngine;

sealed public class Block : MonoBehaviour
{
    [Serializable]
    sealed public class Group
    {
        public Color Color
        {
            get
            {
                return color;
            }
        }

		public Shader NormalShader
		{
			get
			{
				return normalShader;
			}
     	}

        public Shader SpecialShader
        {
            get
            {
                return specialShader;
            }
        }

        public bool IsSelected
        {
            get
            {
                return isSelected;
            }
            
            set
            {
                if (isSelected != value)
                {
                    isSelected = value;

                    SetMaterialSelected(GetNormalMaterial());
                    SetMaterialSelected(GetSpecialMaterial());
                }
            }
        }

        public ParticleEmitter DestructionParticlePrefab
        {
            get
            {
                return destructionParticlePrefab;
            }
        }

        public AudioClip DestructionSound
        {
            get
            {
                return destructionSound;
            }
        }

        public int ChargePointBounty
        {
            get
            {
                return chargePointBounty;
            }
        }

        public float SpecialChance
        {
            get
            {
                return specialChance;
            }
        }

        public BlockAction SpecialAction
        {
            get
            {
                return specialAction;
            }
        }

        public int MemberCount
        {
            get
            {
                return memberCount;
            }

            set
            {
                memberCount = value;
            }
        }
        
        [SerializeField]
        private Color color = Color.white;

		[SerializeField]
		private Shader normalShader = null;

		[SerializeField]
		private Shader specialShader = null;

        private Material normalMaterial = null;
        private Material specialMaterial = null;

        [SerializeField]
        private bool isSelected = false;

        [SerializeField]
        private ParticleEmitter destructionParticlePrefab = null;

        [SerializeField]
        private AudioClip destructionSound = null;

        [SerializeField]
        private int chargePointBounty = 100;

        [SerializeField]
        private float specialChance = 0.05f;

        [SerializeField]
        private BlockAction specialAction = null;

        private int memberCount = 0;

        public void PlayDestructionEffects(Vector3 position)
        {
            ParticleEmitter particleComponent = (ParticleEmitter)Instantiate(destructionParticlePrefab, position, Quaternion.identity);
            particleComponent.renderer.material.SetColor("_TintColor", color);
            
			if (Application.isMobilePlatform)
			{
				particleComponent.minEmission *= 0.5f;
				particleComponent.maxEmission *= 0.5f;
			}

            if (Time.deltaTime > (1.0f / 25.0f))
			{
				particleComponent.minEmission *= 0.25f;
				particleComponent.maxEmission *= 0.25f;
			}

            AudioSource.PlayClipAtPoint(destructionSound, position);
        }

		public Material GetNormalMaterial()
		{
			if (normalMaterial == null || normalMaterial.shader != normalShader)
			{
				normalMaterial = new Material(normalShader);
                normalMaterial.color = color;

                SetMaterialSelected(normalMaterial);
			}

			return normalMaterial;
		}

		public Material GetSpecialMaterial()
		{
			if (specialMaterial == null || specialMaterial.shader != specialShader)
			{
				specialMaterial = new Material(specialShader);
                specialMaterial.color = color;

                SetMaterialSelected(specialMaterial);
			}

			return specialMaterial;
		}

        private void SetMaterialSelected(Material material)
        {
            material.SetFloat(isSelected ? "_SelectTime" : "_DeselectTime", Time.timeSinceLevelLoad);

            if (isSelected && material.HasProperty("_DeselectTime") && material.GetFloat("_DeselectTime") == Time.timeSinceLevelLoad)
            {
                material.SetFloat("_DeselectTime", 0.0f);
            }
        }
    }

    static public Material DefaultMaterial
    {
        get
        {
            if (defaultMaterial == null)
            {
                defaultMaterial = new Material(Shader.Find("Diffuse"));
            }

            return defaultMaterial;
        }

        set
        {
            defaultMaterial = value;
        }
    }

    static private Material defaultMaterial = null;

    public Group CurrentGroup
    {
        get
        {
            return currentGroup;
        }

        set
        {
            if (currentGroup != value)
            {
                if (currentGroup != null)
                {
                    --currentGroup.MemberCount;
                }

                currentGroup = value;

                if (currentGroup != null)
                {
                    ++currentGroup.MemberCount;
                }

                RefreshMaterial();
            }
        }
    }

    public bool IsSpecial
    {
        get
        {
            return isSpecial;
        }

        set
        {
            if (isSpecial != value)
            {
                isSpecial = value;

                RefreshMaterial();
            }
        }
    }

    private Group currentGroup = null;

    private bool isSpecial = false;

    public void RefreshMaterial()
    {
        renderer.sharedMaterial = currentGroup == null ? DefaultMaterial : isSpecial ? currentGroup.GetSpecialMaterial() : currentGroup.GetNormalMaterial();
    }

    private void OnDestroy()
    {
        CurrentGroup = null;
    }
}
