﻿using UnityEngine;

abstract public class BlockAction : MonoBehaviour
{
    abstract public void Perform(Block performer);
}
