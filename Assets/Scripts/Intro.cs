﻿using System;

using UnityEngine;

sealed public class Intro : MonoBehaviour
{
    [Serializable]
    sealed public class Instruction
    {
        public Color Color
        {
            get
            {
                return color;
            }
        }
        public string Content
        {
            get
            {
                return content;
            }
        }
        
        [SerializeField]
        private Color color = Color.white;

        [SerializeField]
        [Multiline]
        private string content = "";
    }

    [SerializeField]
    private GUISkin guiSkin = null;
    
    [SerializeField]
    private Instruction[] instructions = null;

    private int instructionIndex = 0;
    private float instructionShowTime = 0.0f;

    [SerializeField]
    private string nextLevel = null;

    private void Start()
    {
        instructionShowTime = Time.timeSinceLevelLoad;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.Space) || Input.GetMouseButtonDown(0))
        {
            ++instructionIndex;
            instructionShowTime = Time.timeSinceLevelLoad;

            if (instructionIndex >= instructions.Length)
            {
                Application.LoadLevel(nextLevel);
            }
        }
    }

    private void OnGUI()
    {
        GUI.skin = guiSkin;

        if (instructionIndex < instructions.Length)
        {
            GUIStyle textStyle = new GUIStyle(GUI.skin.label);

            textStyle.alignment = TextAnchor.MiddleCenter;
            textStyle.fontStyle = FontStyle.Bold;
            textStyle.fontSize = 64;

            float drawWidth = 600.0f;

            if (drawWidth > Screen.width * 0.9f)
            {
                drawWidth = Screen.width * 0.9f;
            }

            GUI.color = new Color(instructions[instructionIndex].Color.r, instructions[instructionIndex].Color.g, instructions[instructionIndex].Color.b, Time.timeSinceLevelLoad - instructionShowTime);
            GUI.Label(new Rect(Screen.width * 0.5f - drawWidth * 0.5f, Screen.height * 0.5f - 300.0f, drawWidth, 600.0f), instructions[instructionIndex].Content, textStyle);

            GUI.color = Color.cyan;

            textStyle.alignment = TextAnchor.LowerCenter;
            textStyle.fontStyle = FontStyle.Bold;
            textStyle.fontSize = 24;

            GUI.Label(new Rect(Screen.width * 0.5f - drawWidth * 0.5f, Screen.height - 220.0f, drawWidth, 200.0f), (Application.isMobilePlatform ? "Tap" : "Click or Press Spacebar") + " to Continue", textStyle);
        }
    }
}
