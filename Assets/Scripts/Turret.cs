﻿using UnityEngine;

sealed public class Turret : MonoBehaviour
{
    public float FireRate
    {
        get
        {
            return fireRate;
        }
    }

    public PlayerControl Owner
    {
        get
        {
            return owner;
        }

        set
        {
            owner = value;
        }
    }

    [SerializeField]
    private float fireRate = 1.0f;

    private float fireOffset = 0.0f;

    [SerializeField]
    private Projectile projectilePrefab = null;

    [SerializeField]
    private float orbitSpeed = 10.0f;

    private PlayerControl owner = null;

    private void Start()
    {
        fireOffset = Time.timeSinceLevelLoad % fireRate;
    }

    private void Update()
    {
        if ((Time.timeSinceLevelLoad + fireOffset) % fireRate < Time.deltaTime)
        {
            owner.InstantiatePlayObject(projectilePrefab, transform.position, transform.rotation).Owner = owner;
        }

        if (orbitSpeed > 0.0f)
        {
            float localMagnitude = transform.localPosition.magnitude;

            transform.localPosition = Vector3.zero;
            transform.localRotation = Quaternion.Euler(0.0f, orbitSpeed * Time.deltaTime, 0.0f) * transform.localRotation;

            transform.localPosition = (transform.localRotation * -Vector3.forward) * localMagnitude;
        }
    }
}
